import React from 'react';
import PropTypes from 'prop-types';

const Tareas = ({tarealist, eliminarTarea, FinalizarTarea}) => {
    return(
   
    <div className="cita">

        <p>Tarea:<span>{tarealist.tarea}</span></p>
        <p>iniciativa:<span>{tarealist.iniciativa}</span></p>
        <p>Fecha alta:<span>{tarealist.fecha}</span></p>
        <p>Fecha Resolucion:<span>{tarealist.fechaNecesidad}</span></p>
        <p>Descripcion:<span>{tarealist.descripcion}</span></p>
        <div className="row">
        <button className="button is-danger is-rounded"
         onClick={() => eliminarTarea(tarealist.id)}>
            Eliminar
        </button>
        
        <button className="button is-success is-rounded"
            onClick={() => FinalizarTarea(tarealist.id)}>
            Finalizar
        </button>
        </div>
       
    </div>
    );
}

Tareas.propTypes = {
    tarealist: PropTypes.object.isRequired,
    eliminarTarea: PropTypes.func.isRequired,
    FinalizarTarea: PropTypes.func.isRequired
  }
export default Tareas;
