import React, {Fragment, useState} from 'react';
import uuid from 'uuid/v4';
import PropTypes from 'prop-types';
import styled from '@emotion/styled';



const Tituloh2 = styled.h2`
font-size: 18px;
color: #f5f1ed;
padding-botom:3rem;
`;


const Formulario = ({creaTarea}) => {
//creo el state inicial de tareas

const [tareaIni, setTareaIni] = useState ({
    tarea:'',
    iniciativa:'',
    fecha:'',
    fechaNecesidad:'',
    fechaReal:'',
    descripcion:''

});

const [error, setError] = useState(false);

const handleChange = (e) =>{
//utilizo el setTarea para actualizar el state inicial
setTareaIni({
      ...tareaIni,
      [e.target.name]: e.target.value
  })
    
}

//extraigo los valores aplicando distructuring

const { tarea, iniciativa, fecha, fechaNecesidad, descripcion} = tareaIni

//cuando el usuario envia los datos
const submiteForm = (e) => {
// permite controlar las acciones del submit para que no se envien
    e.preventDefault();
//validar  || = or
  if(tarea.trim()=== '' || iniciativa.trim()=== '' || fecha.trim()=== '' ||
     fechaNecesidad.trim()=== '' || descripcion.trim()=== '') {
      setError(true);
      return;
  }
//Si pase la validacion pongo a false el error
    setError(false);
//Asignar un ID como es un objeto puedo agregar otros campos
    tareaIni.id= uuid();
   
//Crear tarea
creaTarea(tareaIni);

//Reiniciar el form cada vez que se agrega infor y se envia
//para que se inicialicen los campos se usa el value con el campos al que hace referencia
setTareaIni({
    tarea:'',
    iniciativa:'',
    fecha:'',
    fechaNecesidad:'',
    fechaReal:'',
    descripcion:''
});

}
    return ( 
        <Fragment>
           <div className="container">
           {error ? <h2 className="alerta-error">Todos los campos son Obligatorios</h2>
                  : <Tituloh2>Crear nuevas tareas</Tituloh2>}
           </div>
           <form 
              onSubmit={submiteForm}
           >
              <br></br>
               <label>Nombre de tarea</label>
               <input
                    type="text"
                    name="tarea"
                    className="u-full-width"
                    placeholder="Ingrese nueva tarea"
                    onChange={handleChange}
                    value={tarea}

               />
 
        
               <label>Iniciativa Asociada</label>
               <input
                    type="text"
                    name="iniciativa"
                    className="u-full-width"
                    placeholder="Iniciativa asociada"
                    onChange={handleChange}
                    value={iniciativa}
               />
               <label>Fecha de alta</label>
                <input
                    type="date"
                    name="fecha"
                    className="u-full-width"
                    onChange={handleChange}
                    value={fecha}
               />
               
               <label>Fecha de Necesidad</label>
                <input
                    type="date"
                    name="fechaNecesidad"
                    className="u-full-width"
                    onChange={handleChange}
                    value={fechaNecesidad}
               />
               
               <label>Descripcion Tarea</label>
                <textarea
                    name="descripcion"
                    className="u-full-width"
                    onChange={handleChange}
                    value={descripcion}
               >                   
               </textarea>
               <button
                type="submit"
                className="u-full-width button-primary">
                    Agregar Tarea
                </button>
           </form>


        </Fragment>
        
     );
}
 //en este caso se documento con proptype para saber el tipo de dato
Formulario.propTypes = {
    creaTarea: PropTypes.func.isRequired
  }
  
export default Formulario;