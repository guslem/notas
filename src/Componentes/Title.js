import React from 'react';
/*este objeto titulo sera reusable
vamos a exportar una arrow function que devovlera
las propiedades del titulo
*/
/*
al exportar un elemento como default se puede importar con cualquier nombre
en cambio si lo hacemos nombrado siempre se importara igual
PAra esto debemos convertir el nombre en una constante

export default ({children}) => {
    <h1 class="title">{children}</h1>    
}

export const Title= ({children}) => {
    <h1 class="title">{children}</h1>    
}

el uso de la palabla class(javascript) esta reservado por eso se debe utilizar className(html)
para no colicionar con javascrip
las arrow function siempre van entre parentesis
*/

export const Title = ({ children }) => (
    <h1 className="title" > { children } </h1>    
)

export const Subtitle = ({ children }) => (
<h2 className="subtitle">{children}</h2>
)

export const Subtitle2 = ({ children }) => (
    <h3 className="subtitle2">{children}</h3>
    )
