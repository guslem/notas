import React, {Fragment, useState, useEffect} from 'react';
import Formulario from './Componentes/Formulario';
import Tareas from './Componentes/Tareas';
import TareasFin from './Componentes/TareasFin';
import 'bulma/css/bulma.css';
import uuid from 'uuid/v4';
import styled from '@emotion/styled';
//import {Title} from './Componentes/Title';

const Tituloh2 = styled.h2`
font-size: 18px;
color: #f5f1ed;
`;
const Tituloh1 = styled.h1`
font-size: 23px;
color: #f5f1ed;
`;



function App() {
// obtengo el resultado guardado si es que lo hay, como el local solo guarda string convierto el array en un json
// como el contenido cambia uso let
let tareasIniciales =  JSON.parse(localStorage.getItem('tareasInis'))
//si no hay tareas inicia con un array vacio
if (!tareasIniciales) {
  tareasIniciales= [];

}

let tareasFinalizadas =  JSON.parse(localStorage.getItem('tareasFinis'))
//si no hay tareas inicia con un array vacio
if (!tareasFinalizadas) {
  tareasFinalizadas= [];

}

  //------------------------------> como guardo los elementos en localStorage cuando inicia la app busca lo que hay
  //                                guardado y arma el state inicia
//agrego el arreglo para contener todas las tareas
const [tareasInis, setTareasInis] = useState (tareasIniciales);

//agrego arreglo de finalizadas
const [tareasFinis, setTareasFinis] = useState (tareasFinalizadas);



useEffect ( () => {
  //si hay tareas las cargi en memoria y realiso el parse de texto a array
if (tareasIniciales){
  localStorage.setItem('tareasInis',JSON.stringify(tareasInis))
} else {
  localStorage.setItem('tareasInis',JSON.stringify([]))
}
 
}, [tareasInis, tareasIniciales] );




useEffect ( () => {
  //si hay tareas las cargi en memoria y realiso el parse de texto a array
if (tareasFinalizadas){
  localStorage.setItem('tareasFinis',JSON.stringify(tareasFinis))
} else {
  localStorage.setItem('tareasFinis',JSON.stringify([]))
}
 
}, [tareasFinis, tareasFinalizadas] );


//funcion que toma la tarea actual y la guarda, viene en las props que le paso al Formulario
const creaTarea = (tareaIni) => {
   setTareasInis([
     ...tareasInis,
         tareaIni])
  }

//funcion que toma la tarea actual y la guarda, viene en las props que le paso al Formulario
const creaTareaFin = (tareaFin) => {
  setTareasFinis([
    ...tareasFinis,
        tareaFin])
 }



  //paso el valor del id a la function, creo un nuevo arreglo para filtrar
  //luego me quedo con todos los resultados distintos(elimina el igual)
  //despues guardo en el state el nuevo arreglo
  const eliminarTarea = (id) => {
    //uso filter para buscar dentro del array
  const nuevasTareas = tareasInis.filter(tareaIni => tareaIni.id !== id)
  setTareasInis(nuevasTareas);

}

  //paso el valor del id a la function, creo un nuevo arreglo para filtrar
  //luego me quedo con todos los resultados distintos(elimina el igual)
  //despues guardo en el state el nuevo arreglo
  const eliminarTareaFin = (id) => {
    //uso filter para buscar dentro del array
    const nuevasTareasF = tareasFinis.filter(tareaFin => tareaFin.id !== id)
    setTareasFinis(nuevasTareasF);


}

//Al finalizar la paso al otro repositorio
const FinalizarTarea = (id) => {
  //me quedo con la igual
  const nuevasTareasFin =   tareasInis.filter(tareaIni => tareaIni.id === id) 
  eliminarTarea(id);
  nuevasTareasFin.id =  uuid();
  creaTareaFin(nuevasTareasFin);

 
}
//creo un titulo dinamico
const titulo = tareasInis.length === 0 ? "No tenes tareas" : "Tareas pendientes"
const titulo1 = tareasFinis.length === 0 ? "No tenes tareas finalizadas" : "Tareas finalizadas"

  return (
 //Utilizo un fragment que me permite devolver mas de un componente en el return
//el framework de skeleton tiene diferentes className igual a bulma o boostrap
    <Fragment>
    <Tituloh1>Administrador de Tareas</Tituloh1>
    <div>
      <div className="row">
         <div className="columna">
          <Tituloh2>{titulo1}</Tituloh2>
            {tareasFinis.map(tareaFin=> (
            <TareasFin
              key={tareaFin.id}
              tarealistFin={tareaFin}
              eliminarTareafin={eliminarTareaFin}
            />
            ))}
           </div>
           
           <div className="columna">
               <Formulario
               creaTarea={creaTarea}/>
          </div>
          <div className="columna">
          <Tituloh2>{titulo}</Tituloh2>
            {tareasInis.map(tarea => (
            <Tareas
              key={tarea.id}
              tarealist={tarea}
              eliminarTarea={eliminarTarea} 
              FinalizarTarea={FinalizarTarea} 
            />

            ))}
          </div>
  
        </div>
    </div>
    </Fragment>
  );
}


export default App;
